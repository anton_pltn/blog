<?php

namespace IteaProject\anton_pltn\Lesson_11\Service;

use IteaProject\anton_pltn\Lesson_11\Entity\Article;
use IteaProject\anton_pltn\Lesson_11\Repository\ArticleRepository;

class ArticleManager
{
    /**
     * @ $articleRepository ArticleRepository
     */
    private $articleRepository;

    public function __construct($articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function saveArticle(Article $article)
    {
        $this->articleRepository->saveArticle();
    }

    public function getArticleFromOrder($order)
    {

    }
}
<?php

namespace IteaProject\anton_pltn\lesson_11\Repository;

use Antonpleteniy\Db_connector_ant\Service\Helpers\DataMapper;
use Antonpleteniy\Db_connector_ant\Service\MysqlConnectionService;

class Repository
{
    private $mysqlConnectionService;

    public function __construct(MysqlConnectionService $mysqlConnectionService)
    {
        $this->mysqlConnectionService = $mysqlConnectionService;
    }

    public function insert($object)
    {
        return $this->mysqlConnectionService->insert($object);
    }

    public function update($object)
    {
        return $this->mysqlConnectionService->update($object);
    }

    public function delete($object)
    {
        return $this->mysqlConnectionService->delete($object);
    }

    public function select($object, $params)
    {
        $array = $this->mysqlConnectionService->select($object, $params);

        $dataMapper = new DataMapper;

        return $dataMapper->arrayToData($object, $array[0]);
    }
}

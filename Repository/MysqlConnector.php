<?php 
namespace IteaProject\anton_pltn\lesson_11\Repository;

use Antonpleteniy\Db_connector_ant\Service\MysqlConnectionService;

class MysqlConnector extends MysqlConnectionService 
{

    public function update($object)
    {
       
         $tableName = $this->sqlHelper->getTableNameFromClass($object);
         $columns = $this->sqlHelper->getColumns($object, true);
         $params = $this->sqlHelper->getParams($columns['columns']);
         $idToUpdate = $object->getId();

         $types = $this->sqlHelper->getTypes($columns['values']);

         $stmt = $this->connection->stmt_init();

        

         $sql = sprintf('UPDATE %s SET %s where id = %s', $tableName, implode(' = ?,', $columns['columns']) . ' = ?',  $idToUpdate);

        // dump($sql);
        // exit;

         $stmt->prepare($sql);
         $stmt->bind_param($types, ...$columns['values']);

         return $stmt->execute();
    }

    public function delete($object)
    {
        // $tableName = $this->sqlHelper->getTableNameFromClass($object);
        // $idToDelete = $object->getId();

        // $stmt = $this->connection->stmt_init();

        // $sql = sprintf('DELETE FROM %s WHERE id = ?', $tableName);

        // $stmt->prepare($sql);

        // $stmt->bind_param('i', $idToDelete);


        // return $stmt->execute();
    }

    public function findAll($object)
    {
        // $tableName = $this->sqlHelper->getTableNameFromClass($object);
    }
    
}

<?php

namespace IteaProject\anton_pltn\lesson_11\Controller;

use Antonpleteniy\Db_connector_ant\Service\MysqlConnectionService;
use DateTime;
use IteaProject\anton_pltn\Lesson_11\Entity\Article;
use IteaProject\anton_pltn\Lesson_11\Repository\ArticleRepository;
use IteaProject\anton_pltn\lesson_11\Repository\MysqlConnector;

class ArticleController
{

    private $mysqlConnectionService;

    private $articleRepository;

    public function __construct()
    {
        $this->mysqlConnectionService =  MysqlConnector::getInstance(
            'localhost',
            'root',
            'root',
            'itea_project'
        );

        $this->articleRepository = new ArticleRepository($this->mysqlConnectionService);
    }



    public function actionCreate()
    {

        $article = new Article;

        $article->setText($_POST['text'])
            ->setTitle($_POST['title'])
            ->setCreatedAt((new \DateTime())->format('Y-m-d H:i:s'))
            ->setPublished(isset($_POST['published']));



        echo '<pre>';
        var_dump($article);
        echo '</pre>';


        $this->articleRepository->insert($article);

        echo $article->getTitle();
    }

    public function actionShow($id)
    {
        $article = $this->articleRepository->select(new Article, ['id' => $id]);

        return $article->getTitle();
    }

    public function actionUpdate($id)
    {
        $article = $this->articleRepository->select(new Article, ['id' => $id]);
        


        $article->setText($_POST['text'])
            ->setTitle($_POST['title'])
            ->setUpdatedAt((new \DateTime())->format('Y-m-d H:i:s'))
            ->setPublished(isset($_POST['published']));


        $this->articleRepository->update($article);


        // dump($article);
        // exit;

        return $article->getTitle();
    }

    public function actionDelete($id)
    {
    }
}

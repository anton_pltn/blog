<?php

require_once "../../vendor/autoload.php";
//require_once "vendor/autoload.php";
//require_once '/opt/homebrew/var/www/itea_project/autoload.php';


/**
 * variant 1 routing page 
 */
/*
switch ($_GET['page']) {
    case 'actionCreate':
        $controller = new \IteaProject\anton_pltn\lesson_11\Controller\ArticleController();

        $controller->actionCreate([]);
        break;
    case 'articleShow':
        $controller = new \IteaProject\anton_pltn\lesson_11\Controller\ArticleController();

        $controller->actionShow([37]);
        break;

    default:
        throw new Exception('not found', 404);
}
*/


/**
 * variant 2 routing page 
 */

use IteaProject\anton_pltn\lesson_11\Controller\ArticleController;
use Symfony\Component\Routing\{Route, RequestContext, RouteCollection };
use Symfony\Component\Routing\Matcher;


$route = new Route('article/show', ['_controller'  => ArticleController::class]);

$routes = new RouteCollection();
$routes->add("article_show", $route);



$context = new RequestContext();

//$matcher = new Matcher\UrlMatcher($routes, $context);

//dump($_SERVER);
